<?php

require_once 'vendor/autoload.php';

$app = new \Slim\Slim();

//$db = new mysqli('localhost','id7450680_edison','admin1234','id7450680_appdb');
$db = new mysqli('localhost','root','','appdb');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

$app->get('/list', function() use($db, $app){
    $sql = 'SELECT * FROM categorias';
    $query = $db->query($sql);

    $datos = array();
    while($dato = $query->fetch_assoc()){
        $datos[] = $dato;
    }

    echo json_encode($datos);
});

// Listar todos los  datos de una categoria
$app->get('/list/:categoria', function($categoria) use($db, $app){
    $sql = 'SELECT * FROM '. $categoria;
    $query = $db->query($sql);

    $datos = array();
    while($dato = $query->fetch_assoc()){
        $datos[] = $dato;
    }

    echo json_encode($datos);
});

//Iniciar sesión
$app->post('/login', function() use($db, $app){
    if(!isset($_POST['email'])){
        $email = null;
    }

    if(!isset($_POST['pass'])){
        $pass = null;
    }

    $email = $_POST['email'];
    $pass = $_POST['pass'];

    $query = "SELECT * FROM login WHERE email = '{$email}' AND pass = '{$pass}' LIMIT 1";

    $login = $db->query($query);
    
    if($login->num_rows == 1){
        $datos = $login->fetch_assoc();
        
        $result = array(
            'status' => 'success',
            'code' => 200,
            'data' => $datos
        );
    } else {
        $result = array(
            'status' => 'error',
            'code' => 404,
            'data' => 'Los datos no son válidos.'
        );
    }

    echo json_encode($result);
});

//Registrar usuario
$app->post('/add-user', function() use($app, $db) {
    
    if(!isset($_POST['nombre'])){
        $nombre = null;
    }

    if(!isset($_POST['email'])){
        $email = null;
    }

    if(!isset($_POST['pass'])){
        $pass = null;
    }

    $nombre = $_POST['nombre'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    
    $sql = "SELECT * FROM login WHERE email = '{$email}'";

    $query = $db->query($sql);

    if($query->num_rows == 0) {
        $sql2 = "INSERT INTO login (nombre,email,pass) VALUES ('{$nombre}', '{$email}', '{$pass}');";
        $insert = $db->query($sql2);
        if($insert){
            $result = array(
                "status" => "success",
                "code" => 200,
                "data" => "Registro exitoso."
            ); 
        }
               
    } else {
        $result = array(
            "status" => "error",
            "code" => 404,
            "data" => "Este usuario ya existe en nuestra base de datos."
        ); 
    }

    echo json_encode($result);
});

// Agregar favorito
$app->post('/add', function() use($app, $db) {
    $item = $_POST['item'];
    $idCategoria = $_POST['categorias_id'];
    $idUser = $_POST['login_id'];

    $query = "INSERT INTO FAVORITOS VALUES (NULL,". $item .", ".$idCategoria.",". $idUser .");";

    $insert = $db->query($query);

    if($insert){
		$result = array(
			'status' => 'success',
			'code'	 => 200,
			'message' => 'Favorito agregado correctamente.'
		);
	} else {
        $result = array(
			'status' => 'error',
			'code'	 => 404,
			'message' => 'Favorito no se ha podido agregar.'
		);
    }
});

//Eliminar favoritos
$app->get('/delete-favorites/:id', function($id) use($app, $db){
    $sql = 'DELETE FROM favoritos WHERE id = ' . $id;
    $query = $db->query($sql);
    if($query) {
        $result = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Favorito retirados con éxito.'
        );
    } else {
        $result = array(
            'status' => 'error',
            'code' => 404,
            'message' => 'Favorito no se ha podido retirar.'
        );
    }

    echo json_encode($result);
});
$app->run();
